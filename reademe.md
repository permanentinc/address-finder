Place Finder
============

A address finder widget that uses Google's geocoder service to return formatted address results.

Author - [Jaydn de Graaf](http://permanentinc.me)
Basic Usage
-----------

Place finder is easy to use. Just include the google maps API, target any element on the page, and create a new instance of  `placeFinder` on it.


### HTML

``
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&address=nz&sensor=false"></script>
``

### JavaScript

``
var addressFinder = new PlaceFinder(document.getElementById("address-finder"));
        document.addEventListener("pfResult", function (response) {
          //Do stuff here with response.detail
        }, false);
``

Returned Results
----------------

country: "NZ"

+ fullAddress: "50 Remuera Road, Remuera 1050, New Zealand"

+ latitude: -36.8708373

+ longitude: 174.7788706

+ number: "50"

+ postalCode: "1050"

+ region: "Auckland"

+ street: "Remuera Rd"

+ suburb: "Remuera"
