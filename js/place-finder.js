//-------------------------------------------------------------------------------------------------------\\
//
// PLACE FINDER | Address geocoding widget - Author Jaydn de Graaf
//
//  An address geocoding widget that takes a string from an input field and returns a preoperly `formatted
//  address response.
//
//  Filtration of address responses
//
//  TODO Need to refactor the keyup functionality to make sure that there are no instances of a non result 
//  when a result should technically be present. This happens when a user is too quick whilst typing in a
//  desired address. Possible solution would be to perform a timout function on the keyup event listener 
//  as opposed to the keydown. So the results come after the user has finished typing. ie 200ms
//
//-------------------------------------------------------------------------------------------------------\\

var geocoder = new google.maps.Geocoder();

function PlaceFinder(element, options) {

    "use strict";

    // Create the needed varibles
    var widget = element,
    addresses = {},
    index = -1,

    // Default options or their respective overrides
    config = {
        timer: null,
        keyPressTimeout: (options.keyPressTimeout || 400),
        resultsPosition: (options.resultsPosition || 'bottom'),
        maxResults: (options.maxResults || 10),
        country: (options.country ||'NZ'),
        hideOnBlur: (options.hideOnBlur || true),
        showOnFocus: (options.showOnFocus || true),
    };

    // Construct the widget
    this.init = function () {
        var div = document.createElement('div');
        div.className = 'pf-container';
        div.innerHTML = '<input class="pf-input" type="text" name="name">';
        div.innerHTML += '<ul class="pf-results"></ul>';
        widget.appendChild(div);
    };

    // Initialise the widget
    this.init();

    var input = widget.querySelector('input');
    var list = widget.querySelector('ul');

    if(options.placeholder){
        input.placeholder = options.placeholder;
    }

    var setGeoBounds = function () {
        var geo_bounds, ne, sw;
        geo_bounds = new google.maps.LatLngBounds();
        sw = new google.maps.LatLng(-45.829174, 164.050803);
        ne = new google.maps.LatLng(-33.578464, -178.766580);
        geo_bounds.extend(sw);
        geo_bounds.extend(ne);
        return geo_bounds;
    };

    var geocodeValue = function (string) {
        geocoder.geocode({
            'address': string + ', ' + config.country,
            bounds: setGeoBounds()
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                config.timer = null;
                index = -1;
                addresses = results;
                list.innerHTML = '';
                for (var i = 0; i < results.length; i++) {
                    if (results[i].address_components.length < 2) {
                        list.innerHTML = '';
                    } else {
                     list.innerHTML += '<li>' + results[i].formatted_address + '</li>';
                 }
             }
             list.className = "active";
         } else {
            list.innerHTML = '';
        }
    });
    };

    var populateAddress = function (selectedResult) {

        // Make sure that there is an address to process
        if (addresses.length > 0) {

            // Small catch to see if there is a selected list item, otherwise set it to the first available one
            selectedResult = (selectedResult > -1) ? selectedResult : 0;

            //Put the response into a variable
            var response = addresses[selectedResult];

            // Get the persistent fields and populate their respective objects' propeties
            var address = {
                fullAddress: response.formatted_address, // Full address with all components
                latitude: response.geometry.location.k, // Address latitude
                longitude: response.geometry.location.A // Address longitude
            };

            // Fill in the individual components for the selected response
            for (var i = 0; i < response.address_components.length; i++) {
                var $this = response.address_components[i];
                switch ($this.types[0]) {
                    case "locality":
                    address.city = $this.short_name;
                    break;
                    case "sublocality":
                    address.suburb = $this.short_name;
                    break;
                    case "route":
                    address.street = $this.short_name;
                    break;
                    case "administrative_area_level_1":
                    address.region = $this.short_name;
                    break;
                    case "country":
                    address.country = $this.short_name;
                    break;
                    case "street_number":
                    address.number = $this.short_name;
                    break;
                    case "postal_code":
                    address.postalCode = $this.short_name;
                    break;
                }
            }

            input.value = address.fullAddress;

            destroyResults();

            var pfResult = new CustomEvent(
                "pfResult",{
                    detail: address,
                    bubbles: true,
                    cancelable: true
                });

            widget.dispatchEvent(pfResult);

        }
    };

    // Dstroy any results list htat has been created
    var destroyResults = function () {
        list.className = 'inactive';
        setTimeout(function () {
            list.innerHTML = '';
        }, 480);
        config.currentItem = -1;
        var pfDestroy = new CustomEvent(
            "pfDestroy",
            {
                detail: 'listDestroyed',
                bubbles: true,
                cancelable: true
            }
            );
        widget.dispatchEvent(pfDestroy);
    };

    // Select the active state list item and return it's respective results
    var highlight = function (index, previous) {
        if (addresses.length > 0) {
            var selected = list.getElementsByTagName('li');
            selected[index].classList.toggle("hover");
            if (previous !== 0) {
                selected[index + previous].classList.toggle("hover");
            }
        }
    };

    list.addEventListener('click', function (e) {
        e.stopPropagation();
        getIndex(e.target);
    }, false);

    // Get the currently selected item in the returned list
    function getIndex(elem) {
        var selectedIndex = 0;
        while (elem.previousSibling) {
            selectedIndex++;
            elem = elem.previousSibling;
        }
        populateAddress(selectedIndex);
    }

    // Optional destruction of the list on blur
    input.onblur = function (e) {
        if(options.hideOnBlur === true && e.target !== list) {
            destroyResults()
        }
    };  

    // Optional recreation of the list when the input is re-focused
    input.onfocus = function () {
        if (options.showOnFocus === true && this.value) {
            geocodeValue(this.value);
        }
    };

    // Only commit to a search if the user has typed at least one letter
    function hasChar(str) {
        return /.*[a-zA-Z].*$/.test(str);
    }

    // Main keyup functionality with the optional keypress timeout
    widget.onkeyup = function (event) {
        var key = event.keyCode;
        var direction;
        // If 'Enter' key is pressed
        if (key === 13) {
            populateAddress(index);
        }
        // If 'Esc' key is pressed
        else if (key === 27) {
            destroyResults();
        }
        // If 'up' or 'left' key is pressed
        else if (key === 38 || key === 37) {
            if (index === 0) {
                return false;
            } else {
                index = index - 1;
                direction = 1;
            }
            highlight(index, direction);
        }
        // If 'down' or 'right' key is pressed
        else if (key === 40 || key === 39) {
            index = index + 1;
            if (index >= addresses.length) {
                index = addresses.length - 1;
                return false;
            } else if (index < 1) {
                direction = 0;
            } else {
                direction = -1;
            }

            // Select or unselect the indexed item in the returned list item
            highlight(index, direction);
        }
        // Actual instantiation of the geocoding service
        else {
            var value = input.value;
            if (config.timer !== null) {
                clearTimeout(config.timer);
            }
            // Optional timeout so as to not return any errors from the geocoding service
            setTimeout(function () {
                if (hasChar(value)) {
                    geocodeValue(value);
                }
            }, config.keyPressTimeout);
        }

    };

}
